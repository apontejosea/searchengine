import os
from unittest import mock

import requests
from unittest import TestCase
from unittest.mock import MagicMock
from datetime import timedelta

from searchengine.text_finder import TextFinder
from searchengine.html_store import HtmlStore

print(os.environ)


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data):
            for k in json_data:
                setattr(self, k, json_data[k])

    return MockResponse({
        'url': args[0],
        'reason': 'OK',
        'status_code': 200,
        'text': 'html text',
        'ok': True,
        'elapsed': timedelta(seconds=5),
        'content': b'content',
        'encoding': 'utf-8'
    })


class TestUrlParser(TestCase):
    def test_parse_url_correctly(self):
        positive_test_cases = [
            {
                'original': 'fakeurl.com',
                'expected': 'http://fakeurl.com',
            },
            {
                'original': 'http://fakeurl.com',
                'expected': 'http://fakeurl.com',
            },
        ]

        for case in positive_test_cases:
            self.assertEqual(
                TextFinder._parse_url(case['original']),
                case['expected']
            )

    def test_parse_url_raises_error_on_bad_urls(self):
        with self.assertRaises(Exception):
            TextFinder._parse_url('clearly not an url')


class TestSearchText(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_url = 'http://testurl.com'
        cls.test_html = '<html>My testing HTML</html>'
        cls.test_response = {
            'url': cls.test_url,
            'html': cls.test_html,
            'response_status': 200,
            'response_reason': '',
            'request_seconds': 10,
            'error': '',
            'error_message': ''
        }
        requests.get = MagicMock(return_value=cls.test_response)

    def test_search_text_finds_hits_correctly(self):
        searcher = TextFinder(
            text='testing',
            search_url=self.test_url,
            thread_id=0
        )

        self.assertEqual(searcher.search_text(self.test_html), True)

    def test_search_text_doesnt_find_nonexistent_text(self):
        searcher = TextFinder(
            text='non-matching string',
            search_url=self.test_url,
            thread_id=0
        )

        self.assertEqual(searcher.search_text(self.test_html), False)


class TestHtmlStore(TestCase):
    def test_encode_name_encodes_url_successfully(self):
        expected_name = '687474703A2F2F7777772E66616B6575726C2E636F6D'
        self.assertEqual(expected_name, HtmlStore._encode_filename('http://www.fakeurl.com'))

    def test_decode_name_decodes_filename_successfully(self):
        expected_url = 'http://www.fakeurl.com'
        self.assertEqual(expected_url, HtmlStore._decode_url('687474703A2F2F7777772E66616B6575726C2E636F6D'))

    def test_save_html_saves_successfully(self):
        html_dir = os.path.join('data', 'html_store')

        url = 'http://www.fakeurl.com'
        test_html = '''
        <html>
            <head><title>Test HTML</title></head>
            <body>Test HTML</body>
        </html>'
        '''
        HtmlStore._save_html(test_html, url)
        with open(os.path.join(html_dir, '687474703A2F2F7777772E66616B6575726C2E636F6D'), 'r') as file:
            saved_html = file.read()
        self.assertEqual(saved_html, test_html)

        os.remove(os.path.join('data', 'html_store', '687474703A2F2F7777772E66616B6575726C2E636F6D'))

    @mock.patch('html_store.requests.get', side_effect=mocked_requests_get)
    def test_get_remote_html_if_not_stored(self, mock_get):
        # Ensure the html_store file is not here
        test_url = 'http://www.fakeurl.com'
        if HtmlStore._html_exists(test_url):
            print('exists... remove')
            os.remove(os.path.join('data', 'html_store', '687474703A2F2F7777772E66616B6575726C2E636F6D'))

        response = HtmlStore.get(test_url, use_cache=True)
        mock_get.assert_called_with(
            test_url,
            headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
                'Accept-Encoding': 'gzip, deflate',
                'Accept': '*/*', 'Connection': 'keep-alive'
            },
            timeout=20
        )
        self.assertEqual(
            response,
            {
                'url': 'http://www.fakeurl.com',
                'html': 'content',
                'response_status': 200,
                'response_reason': 'OK',
                'request_seconds': 5.0,
                'error': '',
                'error_message': ''
            }
        )

        # Cleaning up
        os.remove(os.path.join('data', 'html_store', '687474703A2F2F7777772E66616B6575726C2E636F6D'))

    def test_get_local_html_if_stored(self):
        test_url = "http://www.fakeurl.com"

        # Save some html locally first
        test_html = '''
        <html>
            <head><title>Test HTML</title></head>
            <body>Test HTML</body>
        </html>'
        '''
        HtmlStore._save_html(test_html, test_url)

        # Get method should read local html, since it was just saved
        self.assertEqual(test_html, HtmlStore.get(test_url)['html'])

        os.remove(os.path.join('data', 'html_store', '687474703A2F2F7777772E66616B6575726C2E636F6D'))
