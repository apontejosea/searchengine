import sys

from setuptools import setup, find_packages


def forbid_publish():
    """
    Prevent accidental publishing or registering to PyPi.
    Background: https://www.tomaz.me/2013/09/03/prevent-accidental-publishing-of-a-private-python-package.html
    """
    argv = sys.argv
    blacklist = ['register']

    for command in blacklist:
        if command in argv:
            values = {'command': command}
            print('Command "%(command)s") has been blacklisted, exiting...' %
                  values)
            sys.exit(2)


forbid_publish()


def parse_requirements():
    with open('requirements.txt', 'r') as file:
        install_requires = [line for line in file.readlines()
                            if not line.strip().startswith('#')
                            and not line.strip().startswith('-')
                            and not line.strip() == '']
        install_requires = [x.replace('\n', '') for x in install_requires]

    return install_requires

setup(
    name='search-engine',
    version='0.0.1',
    description="SearchEngine",
    install_requires=parse_requirements(),
    package_dir={'search_engine': 'searchengine'},
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'seng = searchengine.main:run'
        ]
    }
)
