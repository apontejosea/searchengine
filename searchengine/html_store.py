import sys
import json

import requests
from requests.exceptions import SSLError, ReadTimeout, ConnectTimeout, ConnectionError
from diskcache import Cache
import searchengine.config as config


class HtmlStore:
    cache_expiration = config.Default.cache_expiration

    @classmethod
    def get(cls, url, use_cache):

        headers = requests.utils.default_headers()
        headers.update({'User-Agent': "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0"})

        error_name = ''
        error_message = ''

        with Cache(config.Default.cache_name) as cache:
            if (cached_value := cache.get(url)) and use_cache:
                return json.loads(cached_value)

            for _ in range(config.Default.http_request_max_retries):
                try:
                    print(f'trying: {url}')
                    response = requests.get(url, timeout=config.Default.http_request_timeout, headers=headers)
                    print(f'...finished: {response.status_code} {url}')
                    result = {
                        'url': url,
                        'html': response.content.decode(encoding=response.encoding, errors='ignore'),
                        'response_status': response.status_code,
                        'response_reason': response.reason,
                        'request_seconds': response.elapsed.total_seconds(),
                        'error': error_name,
                        'error_message': error_message,
                    }
                    cache.set(url, json.dumps(result), expire=cls.cache_expiration)
                    return result

                # Errors in which we should immediately break
                except (SSLError, ConnectionResetError) as e:
                    error_name = sys.exc_info()[0].__name__
                    error_message = str(e)
                    break

                # Errors for which we should retry several times
                except (ReadTimeout, ConnectTimeout, ConnectionError) as e:
                    error_name = sys.exc_info()[0].__name__
                    error_message = str(e)

                # For now, don't want to break abruptly on Unhandled exceptions
                except Exception as e:
                    print("Unhandled error")
                    error_name = sys.exc_info()[0].__name__
                    error_message = str(e)
                    break

            print(error_message)

            # We don't want cached errors
            cache.delete(url)

        return {
            'url': url,
            'html': '',
            'response_status': 'Error',
            'response_reason': '',
            'request_seconds': '',
            'error': error_name,
            'error_message': error_message,
        }
