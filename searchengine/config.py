

class Default:
    output_file = 'results.csv'
    cache_expiration = 10 * 60
    cache_name = '.html_cache'
    thread_max_num_connections = 20
    thread_execution_timeout = 20
    http_request_timeout = 10
    http_request_max_retries = 3
