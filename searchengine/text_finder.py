import re
from concurrent.futures import wait
from concurrent.futures.thread import ThreadPoolExecutor

import validators
from urllib3.util import parse_url

import searchengine.config as config
from searchengine.html_store import HtmlStore


class TextFinder:

    @staticmethod
    def _parse_url(url):
        if parse_url(url).scheme is None:
            url = "https://" + url

        if not validators.url(url):
            raise ValueError('Not a valid Url:', url)

        return url

    @classmethod
    def run_search(cls, max_connections, urls, text, use_cache):
        match_results = {}
        urls = [cls._parse_url(url) for url in urls]

        def handle_result(fn):
            # TODO: the html document could be huge. Might want to add some safety code
            #       and split work among several CPUs.

            search_result = re.search(text, fn.result()['html']) if fn.result() else None
            print(search_result)
            is_match = ('Error' if search_result is None else
                        'Yes' if fn.result()['html'] else 'No')
            print(is_match)
            match_results.update({fn.destination: is_match})

        with ThreadPoolExecutor(max_workers=max_connections) as executor:
            futures = {}
            for url in urls:
                futures[url] = executor.submit(HtmlStore.get, url=url, use_cache=use_cache)
                futures[url].destination = url
                futures[url].add_done_callback(handle_result)
            wait(futures.values(), config.Default.thread_execution_timeout)

        return match_results