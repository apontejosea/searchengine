import re

import requests


def get_urls_from_s3():
    print('Reading urls from s3 file...')
    source_text = 'https://s3.amazonaws.com/fieldlens-public/urls.txt'
    text = requests.get(source_text, timeout=15).content.decode()
    text = text.split('\n')

    # Remove header and blank lines
    text.pop(0)
    text.remove('') if '' in text else None

    # Extracting and cleaning up the url column
    urls = [re.sub('"', '', line.split(',')[1]) if len(line) else None for line in text]
    urls.remove(None) if None in urls else None

    return urls