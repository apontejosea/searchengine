import os
import sys
from datetime import datetime
from pprint import pprint as pp

import fire
from pandas import DataFrame

from searchengine.text_finder import TextFinder
from searchengine.utils import get_urls_from_s3
import searchengine.config as config


def main(text='', urls='', max_connections=50, use_cache=True):
    start_time = datetime.now()

    print(f'use cache? {use_cache}')

    # Parameter parsing & validation
    if not len(text):
        print('`text` must not be empty')
        return sys.exit(1)

    if max_connections < 1:
        print('`max_connection` must be greater or equal to 1')

    urls = urls.strip(' ').split(',') if len(urls) else get_urls_from_s3()

    match_results = TextFinder.run_search(max_connections, urls, text, use_cache)

    output_filename = config.Default.output_file
    df = DataFrame(data={'url': match_results.keys(),
                         'is_match': match_results.values()})
    df.to_csv(output_filename, index=False)

    pp(match_results)
    print('===== DONE =====')
    print('Results saved in: ', os.path.join(os.getcwd(), output_filename))
    print('Total time:', datetime.now() - start_time)


def run():
    fire.Fire(main)

if __name__ == '__main__':
    run()