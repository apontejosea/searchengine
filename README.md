## Description

SearchEngine is a simple program to search for a given string in a list of given websites.

## Setup

1.  Install python 3.8+ 

1.  Clone this repository & enter the project directory

    ```
    git clone https://apontejosea@bitbucket.org/apontejosea/searchengine.git
    ```

    ```
    cd searchengine
    ```

1.  *(Optional but recommended)* Create and activate virtual environment

    Assuming a *Linux* environment (Bash)

    ```
    virtualenv .env -p python3
    ```

    ```
    source .env/bin/activate 
    ```

1.  Install dependencies

    ```
    pip install -r requirements.txt
    ```

## Running the program

* Open the shell, navigate to the project directory (and activate the project environment if necessary, as shown above)

* Running the program is just a matter of running a command from the shell. From the project directory, run:

    ```
    python searchengine/main.py --text="<text>" [--max_connections=<max_connections>] [--urls=<urls>] [--use_cache=<use_cache>]
    ```

    where:
    
    * `<text>`: is a string with the text to search in the form of a regular expression.
    * `<max_connections>` (optional): is the maximum number of http connections (20 by default)
    * `<urls>` (optional): is a string with comma-separated list of urls.  If not supplied, the default list will be used.
    * `<use_cache>` (optional): is a boolean.  If *True* (default), a cached html file will be used, if available. If *False*, any cached html would be overwritten.

## Running Tests

* To run tests, just run:

    ```
    pytest
    ```

## TO DO

* ~~Add unit tests~~
* ~~Cache html pages and add option to run additional searches on cached pages~~
* ~~Add support for initial url list to be overridden~~
* ~~Detailed error handling~~
    * ~~SSLError (nothing to do in this case. Log the error and move on.)~~
    * ~~ReadTimeout (Maybe a few retries, limit the timeout to less than a minute)~~
    * ~~ConnectionError~~
    * ~~UnicodeDecodeError (ignore bad characters and just search as usual)~~
* ~~Improve the HtmlStore by the use_cache parameter by an "expiration_time" parameter, where new html will be requested only if it's past expiration.~~

## Wish List

* Increase test coverage, particularly in the area of request error handling.
* Refactor static parameters into a config.
* Optimize http request error handling (e.g. how much is enough timeout? how many retries are being executed by requests.get internally per error type?)

## Who do I talk to?

**Jose Aponte** (apontejosea@gmail.com)
